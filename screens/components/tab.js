import React,{useRef} from 'react';
import styled from 'react-native-styled-components';
import { StyleSheet, TouchableWithoutFeedback, Text, View, Image } from "react-native";
//import { Ionicons } from '@expo/vector-icons';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { FontAwesome5 } from '@expo/vector-icons';
import {Transition,Transitioning} from 'react-native-reanimated';

var colour;
var nm;
var Tag;
var backgroundColour, textColour;

function Tab({lable,accessibilityState, onPress}){

    const focused = accessibilityState.selected;
   
  if(lable=='account-search')
  {
    nm='Search';
    Tag=MaterialCommunityIcons;
    if(focused)
    {
      colour='#2d9cdb';
      backgroundColour='#bce3fa';
      textColour='#2d9cdb';
    }
    else
    {
      colour='#4a4a4a';
      backgroundColour='white';
    }
  }
  else{
    nm='My Add';
    Tag=FontAwesome5;
    if(focused)
    {
      colour='#c56b14';
      backgroundColour='#ffe1c5';
      textColour='#c56b14';
    }
    else
    {
      colour='#4a4a4a';
      backgroundColour='white';
    }
  }

  const colorStyles = {
    backgroundColor: backgroundColour
  };

  const trasition =(
    <Transition.Sequence>
      <Transition.Out type="fade" durationMs={0} />
      <Transition.Change interpolation="easeInOut" durationMs={100} />
      <Transition.In type="fade" durationMs={10} />
    </Transition.Sequence>
  );

  const ref= useRef();

   
    return(
    <TouchableWithoutFeedback onPress={
    //  ref.current.animateNextTransition();
      onPress
     }>
      {/* <Transitioning ref={ref} trasition={trasition}> */}
      <View style={styles.container} >
        
        <View style={[styles.text,colorStyles]} >
            <Tag name={lable} size={26} color={colour} />
                {focused && <Text style={[styles.lableText,{color:textColour}]}>   {nm}</Text>}
        </View>

   </View>
      {/* </Transitioning> */}
    </TouchableWithoutFeedback>

    );
}

export default Tab;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection:"row",
    alignItems:'center',
    justifyContent: 'space-around',
  
  },

  text: {
    flex:1,
    flexDirection:"row",
    alignItems:'center',
    justifyContent: 'center',
    backgroundColor:'red',
    borderRadius:100,
    margin: 32,
    paddingTop:3,
    paddingBottom:3,

  },

  lableText:{
    fontWeight: "900",
    fontSize: 16,
  },
});

