import React from 'react';
import { Text, View, Image, StyleSheet, Animated } from 'react-native';

class SplashScreen extends React.Component{

    constructor(props)
    {
        super(props);
        setTimeout(() => {
            this.props.navigation.navigate('login');
            }, 3000);
    }


   state={
       LogoAnimation: new Animated.Value(0),
       LogoText: new Animated.Value(0),
       landingSpinner:false,
   }

   componentDidMount(){
       const{LogoText,LogoAnimation} = this.state;
       Animated.parallel([
           Animated.spring(LogoText, {
               toValue:1,
               tension: 10,
               friction: 2,
               duration:1000,
               useNativeDriver: false,
           }).start(),

           Animated.timing(LogoAnimation, {
               toValue: 1,
               duration: 2000,
               useNativeDriver: false,
           }).start(() => {
               this.setState({
                   landingSpinner: true,
               });
           }

           )
       ])
   }

    render(){
        return(
            <View style={styles.container}>

                <Animated.View style={{opacity: this.state.LogoAnimation}} > 

                   <View>
                        <Image source={require('E:/reactApp/tutor/assets/image/logo.png')} style={styles.logo}/>
                  </View> 
                </Animated.View>

                <Animated.View  style={{
                    opacity: this.state.LogoText,
                    top: this.state.LogoText.interpolate({
                        inputRange: [0,1],
                        outputRange: [80,0],
                    })
                }} >

                 <View style={styles.textRow}>
                   <Text style={styles.text}>Tutor</Text>
                   <Text style={styles.aplus}>A+</Text>

                  </View>
                </Animated.View>
               
                </View>
           
        );
    }
}

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#32527b',
    alignItems: 'center',
    justifyContent: 'center',

  },

  text: {
      color: '#FFFFFF',
      fontFamily: 'PTSansCaption-Bold',
      fontSize: 30,
      marginTop: 2,
      fontWeight: '300',
      textAlignVertical: 'center',
     
      
  },
  aplus:{
    color: '#ffcc00',
    fontFamily: 'PTSansCaption-Bold',
    fontSize: 40,
    marginTop: 2,
    fontWeight: '300',
   

  },

  logo: {
    width: 200,
     height: 200,
  },

  textRow: {
      marginTop: 10,
      flexDirection: "row",
  },

});
