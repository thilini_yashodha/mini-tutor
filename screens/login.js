import React, {Component, useState} from 'react';
import { Text, View, StyleSheet, Dimensions, Animated, TouchableWithoutFeedback, Easing, TextInput} from 'react-native';
import { TapGestureHandler } from 'react-native-gesture-handler';
import { Extrapolate } from 'react-native-reanimated';
import Svg,{Image,Circle,ClipPath} from 'react-native-svg';

const {width,height} = Dimensions.get('window');

class Login extends Component{

  constructor(){
    super();

    this.animatedValue = new Animated.Value(1)
  }

  state={
    buttonOpacity: new Animated.Value(1),
  }


  handleAnimation = () => {
    Animated.timing(this.animatedValue, {
        toValue: 0,
        duration: 1000,
        useNativeDriver: false,
        easing: Easing.ease
    }).start()
  }

  onClose = () => {
    Animated.timing(this.animatedValue, {
        toValue: 1,
        duration: 1000,
        useNativeDriver: false,
        easing: Easing.ease
    }).start()
  }

    render(){

  
      function fadeOut() {
        Animated.timing(
          buttonOpacity,
          {
            toValue: 0,
            duration: 1000,
            useNativeDriver: false,
          },
        ).start()

      }

      function fadeIn() {
        Animated.timing(
          buttonOpacity,
          {
            toValue: 1,
            duration: 1000,
            useNativeDriver: false,
          },
        ).start()

      }

      const bgY = this.animatedValue.interpolate({
        inputRange: [0,1],
        outputRange: [-height*0.4 - 50,0]
      });

      const textInputZindex = this.animatedValue.interpolate({
        inputRange: [0,1],
        outputRange: [1,-1],
        extrapolate: Extrapolate.CLAMP,
      });

      const textInputY = this.animatedValue.interpolate({
        inputRange: [0,1],
        outputRange: [0,100],
        extrapolate: Extrapolate.CLAMP,
      });

      const textInputOpacity = this.animatedValue.interpolate({
        inputRange: [0,1],
        outputRange: [1,0],
        extrapolate: Extrapolate.CLAMP,
      });

      const rotateCross = this.animatedValue.interpolate({
        inputRange: [0,1],
        outputRange: ["180deg","360deg"],
        extrapolate: Extrapolate.CLAMP,
      });


    
      let{buttonOpacity}= this.state
      

        return (
            <View style={styles.container}>
              <Animated.View style={[StyleSheet.absoluteFill, {transform:[{scaleY: 1}, {translateY:bgY} ]}]}>
                  
                  <Svg height={height + 50} width={width}>
                    <ClipPath id="clip">
                      <Circle r={height + 50} cx={width/2} />
                    </ClipPath>
                  <Image 
                    href={require('../assets/image/back.jpg')}
                    width={width}
                    height={height + 50}
                    preserveAspectRatio='xMidYMid slice'
                    clipPath="url(#clip)"
                  />
                  </Svg>
                  
              </Animated.View>

              <View style={{height: height*0.4,justifyContent: 'center'}}>

                <TouchableWithoutFeedback onPress={ () => {fadeOut(); this.handleAnimation()}} >
                      <Animated.View style={{...styles.button, opacity:buttonOpacity}}>
                           <Text style={{fontSize: 17, fontWeight: 'bold', alignContent:'center'}}>LOG IN</Text>
                       </Animated.View>
                </TouchableWithoutFeedback>

                <TouchableWithoutFeedback onPress={()=> this.props.navigation.navigate('register')}>
                       <Animated.View style={{...styles.button2, opacity:buttonOpacity}}>
                             <Text style={{fontSize: 17, fontWeight: 'bold'}}>SIGN UP</Text>
                      </Animated.View>
                </TouchableWithoutFeedback>

                  <Animated.View style={{zIndex: textInputZindex ,opacity: textInputOpacity, transform:[{translateY:textInputY}], height:height*0.4, ...StyleSheet.absoluteFill,top:null,justifyContent:'center'}}>
                    
                    <TapGestureHandler onHandlerStateChange={() => {this.onClose(),fadeIn() }}>
                      <Animated.View style={styles.closeBtn}>
                        <Animated.Text style={{fontSize:15, transform: [{rotate: rotateCross }]}}>X</Animated.Text>
                      </Animated.View>
                    </TapGestureHandler>
                    
                    <TextInput placeholder="EMAIL" style={styles.textInput} placeholderTextColor="black"/>
                    <TextInput placeholder="PASSWORD" style={styles.textInput} placeholderTextColor="black"/>

                    <Animated.View style={styles.button3}>
                       <Text style={{fontSize: 20,fontWeight:'bold'}}>LOG IN</Text>
                    </Animated.View> 

                  </Animated.View>
                  
              </View>
            </View>
          );
          
    }
       
}

export default Login;

const styles = StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'flex-end',
      },

      button: {
          backgroundColor: 'white',
          height: 60,
          marginHorizontal:20,
          borderRadius: 35,
          alignItems: 'center',
          justifyContent : 'center',
          marginVertical:8,
          
      },
      button2: {
        backgroundColor: 'white',
        height: 60,
        marginHorizontal:20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent : 'center',
        marginVertical:8,
        shadowColor: "#000",
        shadowOffset: {
               width: 0,
              height: 2,
         },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

         elevation: 5,
    },
      textInput:{
       height:50,
       borderRadius:25,
       borderWidth: 0.5,
       marginHorizontal: 20,
       paddingLeft: 10,
       marginVertical: 5,
       borderColor: 'rgba(0,0,0,0.2)',
      },

      button3: {
        backgroundColor: 'white',
        height: 50,
        marginHorizontal:20,
        borderRadius: 35,
        alignItems: 'center',
        justifyContent : 'center',
        marginVertical:8,
        shadowColor: "#000",
        shadowOffset: {
               width: 0,
              height: 2,
         },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

         elevation: 5,

    },

    closeBtn: {
       height: 40,
       width: 40,
       backgroundColor: 'white',
       borderRadius: 20,
       alignItems: 'center',
       justifyContent: 'center',
       top:-34,
       left: width/ 2 - 10,
       shadowColor: "#000",
        shadowOffset: {
               width: 0,
              height: 2,
         },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

         elevation: 5,

    },

    });