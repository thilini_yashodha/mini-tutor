import React from 'react';
//import { ImageBackground, Text, View, Image, StyleSheet } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import Search from '../screens/search';
import Add from '../screens/add';
import TabComponent from '../screens/components/tab';


const Tabs=createBottomTabNavigator();

export default () => (
 // <NavigationContainer>
  <Tabs.Navigator>
       <Tabs.Screen name= "Search" component={Search}  options={{
         tabBarButton: (props) => <TabComponent lable='account-search' {...props} />,
       }}/>
       <Tabs.Screen name= "Add" component={Add}  options={{
         tabBarButton: (props) => <TabComponent lable='address-card' {...props} />
       }}/>
  </Tabs.Navigator>

 // </NavigationContainer>

)
    
      


// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
