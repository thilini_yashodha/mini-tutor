import React from 'react';
import SplashScreen from '../screens/splashScreen';
import MainScreen from '../screens/mainScreen';
import Login from '../screens/login';
import Register from '../screens/register';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const AuthStack = createStackNavigator();

export default () => (
    <NavigationContainer>
        <AuthStack.Navigator>
            <AuthStack.Screen 
                name="splashScreen"
                component={SplashScreen}
                options={{ headerShown: false }}
            />
             <AuthStack.Screen 
                name="login"
                component={Login}
                options={{headerShown: false}}
            />

            <AuthStack.Screen 
                name="register"
                component={Register}
                options={{headerShown: false}}
            />


        </AuthStack.Navigator>
    </NavigationContainer>
);