import React, { useState } from 'react';
import * as Font from 'expo-font';
import { AppLoading } from 'expo';
import Navigator from './routes/appStack';


const getFont = () => Font.loadAsync({
       'PTSansCaption-Bold': require('E:/reactApp/tutor/assets/font/PT_Sans_Caption/PTSansCaption-Bold.ttf'),

     });
    

export default function App() {

  const[fontsLoaded, setFontsLoaded] =useState(false);

  if(fontsLoaded){
    return (
      <Navigator />
   );
  }
  else{
    return(
      <AppLoading 
      startAsync={getFont}
      onFinish={() => setFontsLoaded(true)}
   />
    )
  }
  
}

